# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]

def depthFirstSearch(problem):
    """
    Search the deepest nodes in the search tree first.

    Your search algorithm needs to return a list of actions that reaches the
    goal. Make sure to implement a graph search algorithm.

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:

    print "Start:", problem.getStartState()
    print "Is the start a goal?", problem.isGoalState(problem.getStartState())
    print "Start's successors:", problem.getSuccessors(problem.getStartState())
    :param problem: SearchProblem object.
    :return: list of actions to goal state.
    :rtype: list()
    """
    states_stack = util.Stack()
    solution_path = list()
    explored_states = set()

    root_state = (problem.getStartState(), solution_path)
    states_stack.push(root_state)

    while not states_stack.isEmpty():
        (current_state, solution_path) = states_stack.pop()

        if problem.isGoalState(current_state):
            return solution_path

        elif current_state not in explored_states:
            explored_states.add(current_state)
            for successor_state, action, cost in problem.getSuccessors(current_state):
                extend_path = solution_path + [action]
                new_state = (successor_state, extend_path)
                states_stack.push(new_state)
    util.raiseNotDefined()

def breadthFirstSearch(problem):
    """Search the shallowest nodes in the search tree first.
    :param problem: SearchProblem object.
    :return: list of actions to goal state.
    :rtype: list()
    """
    states_queue = util.Queue()
    solution_path = list()
    explored_states = set()

    root_state = (problem.getStartState(), solution_path)
    states_queue.push(root_state)

    while not states_queue.isEmpty():
        (current_state, solution_path) = states_queue.pop()

        if problem.isGoalState(current_state):
            return solution_path
        elif current_state not in explored_states:
            explored_states.add(current_state)
            for successor_state, action, cost in problem.getSuccessors(current_state):
                extend_path = solution_path + [action]
                new_state = (successor_state, extend_path)
                states_queue.push(new_state)
    util.raiseNotDefined()

def uniformCostSearch(problem):
    """Search the node of least total cost first.
    :param problem: SearchProblem object.
    :return: list of actions to goal state.
    :rtype: list()
    """
    states_pqueue = util.PriorityQueue()
    solution_path = list()
    solution_cost = 0
    explored_states = set()

    root_state = (problem.getStartState(), solution_path, solution_cost)
    states_pqueue.push(root_state, solution_cost)

    while not states_pqueue.isEmpty():
        (current_state, solution_path, solution_cost) = states_pqueue.pop()

        if problem.isGoalState(current_state):
            return solution_path
        elif current_state not in explored_states:
            explored_states.add(current_state)

            for successor_state, action, cost in problem.getSuccessors(current_state):
                extend_path = solution_path + [action]
                additive_cost = solution_cost + cost
                new_state = (successor_state, extend_path, additive_cost)
                states_pqueue.push(new_state, additive_cost)
    util.raiseNotDefined()

def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def aStarSearch(problem, heuristic=nullHeuristic):
    """Search the node that has the lowest combined cost and heuristic first.
    :param problem: SearchProblem object.
    :param heuristic: heuristic function that takes as arguments a problem state and the problem object itself.
    :return: list of actions to goal state.
    :rtype: list()
    """
    states_pqueue = util.PriorityQueue()
    solution_path = list()
    explored_states = set()
    solution_cost = 0
    heuristic_cost = heuristic(problem.getStartState(), problem)
    total_cost = solution_cost + heuristic_cost

    root_state = (problem.getStartState(), solution_path, total_cost)
    states_pqueue.push(root_state, total_cost)

    while not states_pqueue.isEmpty():
        (current_state, solution_path, solution_cost) = states_pqueue.pop()

        if problem.isGoalState(current_state):
            return solution_path
        elif current_state not in explored_states:
            explored_states.add(current_state)
            for successor_state, action, cost in problem.getSuccessors(current_state):
                extend_path = solution_path + [action]
                additive_cost = solution_cost + cost
                new_heuristic_cost = heuristic(successor_state, problem)
                new_total_cost = additive_cost + new_heuristic_cost
                new_state = (successor_state, extend_path, additive_cost)
                states_pqueue.push(new_state, new_total_cost)
    util.raiseNotDefined()


# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
